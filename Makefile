GOCMD=go

.PHONY: serviceUp
serviceUp:
	@echo "===== RUNNING SERVICES ====="
	docker-compose up -d --build || exit 1

.PHONY: serviceDown
serviceDown:
	@echo "===== DOWN SERVICES ====="
	docker-compose down || exit 1
